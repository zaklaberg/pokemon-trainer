import { NameUrlPair } from "./name-url.model";
import { capitalize } from 'lodash';

export class PokemonBase {
    name: string = '';
    id: number = 0;
    imageUrl: string = '';

    constructor(name: string, id: number) {
        this.name = this.prettify(name);
        this.id = id;
        this.imageUrl = PokemonBase.getImageFromId(id);
    }

    // Capitalizes all words in sentence and removes dashes
    protected prettify(sentence: string) {
        let words: string[] = sentence.split('-');
        words = words.map(word => capitalize(word));
        return words.join(' ');
    }

    static fromNameUrl(res: NameUrlPair): PokemonBase {
        const id: number = PokemonBase.getIdFromUrl(res.url)        
        return new PokemonBase(res.name, id);
    }

    private static getImageFromId(id: number): string {
        return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`
    }

    private static getIdFromUrl(url: string): any {
        const id: number = parseInt(url.split( '/' ).filter(Boolean).pop() || '', 10);
        return id;
    }
}





