import { NameUrlPair } from './name-url.model';

interface ApiAbility {
    ability: NameUrlPair;
    is_hidden: boolean;
    slot: number;
}

interface ApiGameVersion {
    name: string;
    url: string;
}
interface ApiGameIndex 
{
    game_index: number;
    version: ApiGameVersion;
}

interface ApiVersionGroupDetails {
    level_learned_at: number;
    move_learn_method: NameUrlPair;
    version_group: NameUrlPair;
}

interface Move {
    move: NameUrlPair;
    version_group_details: ApiVersionGroupDetails[];
}

interface ApiStat {
    base_stat: number;
    effort: number;
    stat: NameUrlPair;
}

interface ApiType {
    slot: number;
    type: NameUrlPair;
}

export interface DetailsApiResponse {
    abilities: ApiAbility[];
    base_experience: number;
    game_indices: ApiGameIndex[];
    height: number;
    id: number;
    name: string;
    is_default: boolean;
    moves: Move[];
    stats: ApiStat[];
    types: ApiType[];
    weight: number;
}