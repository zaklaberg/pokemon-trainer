import { PokemonBase } from './base.model';
import { DetailsApiResponse } from './details.api-response.model';
import { capitalize } from 'lodash';

// Map to /assets/types3.png
const types3: string[] = ['Bug', 'Grass', 'Fairy', 'Normal', 'Dragon', 'Psychic', 
                            'Ghost', 'Ground', 'Steel', 'Fire', 'Flying', 'Ice', 
                            'Electric', 'Rock', 'Dark', 'Water', 'Fighting', 'Poison' 
                         ];

interface Dictionary<T> {
    [Key: string]: T;
}

const offsets: Dictionary<string> = {};
const INCREMENT_X = 24;
const INCREMENT_Y = 26;
types3.forEach((type: string, i: number) => {
    offsets[type] = `-${INCREMENT_X*(i % 6)}px -${INCREMENT_Y*(Math.floor(i / 6))}px`;
});

export class PokemonDetails extends PokemonBase {
    abilities: string[] = [];
    base_experience: number = 0;
    height: number = 0;
    weight: number = 0;
    moves: string[] = [];
    stats: {
        name: string;
        value: number;
    }[] = [];
    types: string[] = [];
    offsets: Dictionary<string> = {};

    constructor(details: DetailsApiResponse) {
        super(details.name, details.id);

        const { base_experience, height, weight } = details;
        this.base_experience = base_experience;
        this.height = height;
        this.weight = weight;

        this.stats = details.stats.map(stat => ({name: this.prettify(stat.stat.name), value: stat.base_stat}));
        this.abilities = details.abilities.map(ability => this.prettify(ability.ability.name));
        this.types = details.types.map(type => this.prettify(type.type.name));
        this.offsets = offsets;
        this.moves = details.moves.map(move => this.prettify(move.move.name));

        this.stats.forEach(stat => {
            if (stat.name == 'Hp') stat.name = 'HP';
        });
    }
}

