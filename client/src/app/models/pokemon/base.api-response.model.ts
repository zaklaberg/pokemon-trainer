export interface BaseApiResponse {
    success: boolean;
    results: any;
    count: number;
    next: string;
    previous: string;
}