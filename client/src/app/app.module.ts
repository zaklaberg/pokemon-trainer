import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { LoginPage } from './pages/login/login.page';
import { TrainerProfilePage } from './pages/trainer-profile/trainer-profile.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { PokemonDetailsPage } from './pages/pokemon-details/pokemon-details.page';

import { HeaderComponent } from './components/shared/site-header/header.component';
import { BasicCardComponent } from './components/pokemon/card/card.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerProfilePage,
    PokemonCataloguePage,
    PokemonDetailsPage,
    HeaderComponent,
    BasicCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
