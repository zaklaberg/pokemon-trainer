import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    private SESS_USERID_IDENTIFIER = 'pidx-tid';

    get userId(): string { 
        return sessionStorage.getItem(this.SESS_USERID_IDENTIFIER) || '';
    }

    set userId(userId: string) {
        sessionStorage.setItem(this.SESS_USERID_IDENTIFIER, userId);
    }
}