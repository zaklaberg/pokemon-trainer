import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { environment } from 'src/environments/environment';
import { PokemonBase }  from 'src/app/models/pokemon/base.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class TrainerService {
    private ownedPokemon$: BehaviorSubject<PokemonBase[]> = new BehaviorSubject([] as PokemonBase[]);

    constructor(private http: HttpClient, private auth: AuthService) {
        this.fetchPokemon();
    }

    addPokemon(pokemon: PokemonBase) {
        // Check if owned, if so, do not add.

        const url = `${environment.trainerApiUrl}trainer/${this.auth.id}/pokemon`;
        return this.http.post(url, pokemon)
            .pipe(map(
                resp => this.ownedPokemon$.next([pokemon, ...this.ownedPokemon$.value])
            ))
    }

    get ownedPokemon(): Observable<PokemonBase[]> {
        return this.ownedPokemon$.asObservable();
    }

    ownsPokemon(id: number) {
        return this.ownedPokemon$.value.map(poke => poke.id).includes(id);
    }

    private fetchPokemon(): void {
        const url = `${environment.trainerApiUrl}trainer/${this.auth.id}/pokemon`;
        this.http.get<PokemonBase[]>(url)
            .subscribe(
                resp => { this.ownedPokemon$.next(resp) },
                error => { this.ownedPokemon$.next([]) }
            )
    }
}