import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { PokemonDetails } from 'src/app/models/pokemon/details.model';
import { DetailsApiResponse } from 'src/app/models/pokemon/details.api-response.model';
import detailsData from 'src/app/mocks/pokemon-details.mock';

@Injectable({
    providedIn: 'root'
})
export class PokemonDetailsService {
    private currentDetails$: BehaviorSubject<PokemonDetails> = new BehaviorSubject({} as PokemonDetails);

    constructor(private http: HttpClient) {}

    get details(): Observable<PokemonDetails> {
        return this.currentDetails$.asObservable();
    }

    fetchDetailsById(id: number): void {
        //const details: PokemonDetails = new PokemonDetails(detailsData as DetailsApiResponse);
        //this.currentDetails$.next(details as PokemonDetails);

        this.http.get<DetailsApiResponse>(`${environment.pokeApiUrl}pokemon/${id}`)
            .subscribe((resp: DetailsApiResponse) => {
                this.currentDetails$.next(new PokemonDetails(resp))
            })
    }
}