import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PokemonBase } from '../../models/pokemon/base.model';
import { NameUrlPair } from '../../models/pokemon/name-url.model';
import { BaseApiResponse } from '../../models/pokemon/base.api-response.model';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import mockedPokemonList from '../../mocks/pokemon-list.mock';


// TODO: generalize this to depend on a PokemonSourceService, which just delivers (paginated) pokemon.
// Then we can reuse this service to serve both catalogue & profile. And we can reuse catalogue in profile, too.
@Injectable({
    providedIn: 'root'
})
export class PokemonListService {
    private _totalNumberOfPokemon = -1;
    private pokemonPerPage = 30;
    private _nextUrl = `${environment.pokeApiUrl}pokemon?limit=${this.pokemonPerPage}`;
    private _prevUrl = '';
    private currentPokemon$: BehaviorSubject<PokemonBase[]> = new BehaviorSubject([] as PokemonBase[]);

    constructor(private http: HttpClient) {
        // Let's fetch the first [pokemonPerPage] pokemon
        // Also sets maxPokemon to the correct value
        this.next();
    }

    get totalNumberOfPokemon(): number {
        return this._totalNumberOfPokemon;
    }

    get numberOfPages(): number {
        return Math.ceil(this._totalNumberOfPokemon / this.pokemonPerPage);
    }

    current(): Observable<PokemonBase[]> {
        return this.currentPokemon$.asObservable();
    }

    get hasNext(): boolean {
        return this._nextUrl !== '';
    }
    next(): void {
        if (!this.hasNext) return;
        
        this.fetchPokemon(this._nextUrl);
    }

    get hasPrevious(): boolean {
        return this._prevUrl !== '';
    }
    previous(): void {
        if (!this.hasPrevious) return;
        this.fetchPokemon(this._prevUrl);
    }

    private fetchPokemon(url: string): void {
        //this.currentPokemon$.next(mockedPokemonList.map((res: NameUrlPair) => PokemonBase.fromNameUrl(res)));

        this.http.get<BaseApiResponse>(url)
            .pipe(
                map((resp: BaseApiResponse) => {
                    this._totalNumberOfPokemon = resp.count;
                    this._prevUrl = resp.previous || '';
                    this._nextUrl = resp.next || '';
                    return resp.results.map((res: NameUrlPair) => PokemonBase.fromNameUrl(res));
                })
            ).subscribe((newPokemon: PokemonBase[]) => {
                this.currentPokemon$.next(newPokemon);
            })
    }
}