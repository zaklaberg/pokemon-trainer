import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { SessionService } from '../session/session.service';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(
        private http: HttpClient, 
        private sessionService: SessionService,
        private router: Router
    ) { }

    login(name: string, password: string) {
        return this.http.post(`${environment.trainerApiUrl}trainer/add`, {name})
        .pipe(map(
            (resp: any) => {
                this.sessionService.userId = resp.id;
                return {success: true};
            }
        ))
    }

    logout() {
        this.sessionService.userId = '';
        this.router.navigateByUrl('/login');
    }

    get id(): string {
        return this.isLoggedIn ? this.sessionService.userId : '';
    }

    get isLoggedIn(): boolean {
        return this.sessionService.userId !== '';
    }
}