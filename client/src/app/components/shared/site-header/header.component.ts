import { Component } from '@angular/core';
import { Router, NavigationEnd, Navigation } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';
import { filter, map } from 'rxjs/operators';

@Component({
    selector: 'site-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.style.scss']
})
export class HeaderComponent {
    public isLoggedIn: boolean = false;
    private _currentRoute = '/';

    constructor(public authService: AuthService, private router: Router) { 
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
            map(event => event as NavigationEnd)
        ).subscribe(
            evt => this._currentRoute = evt.url, 
            error => {})
    }

    isCurrentRoute(route: string): boolean {
        return this._currentRoute === route;
    }
}