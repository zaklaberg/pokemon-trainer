import { Component, Input } from '@angular/core';
import { PokemonBase } from 'src/app/models/pokemon/base.model';
import { Router } from '@angular/router';

@Component({
    selector: 'basic-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.style.scss']
})
export class BasicCardComponent {
    constructor(private router: Router) { }
    @Input() pokemon: PokemonBase = {} as PokemonBase;

    get name(): string {
        return this.pokemon.name;
    }
    get id(): number {
        return this.pokemon.id;
    }
    get imageUrl(): string {
        return this.pokemon.imageUrl;
    }

    onClick() {
        this.router.navigateByUrl(`/pokemon/${this.id}/details`)
    }
}