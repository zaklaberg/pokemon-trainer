import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './pages/login/login.page';
import { TrainerProfilePage } from './pages/trainer-profile/trainer-profile.page';
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { PokemonDetailsPage } from './pages/pokemon-details/pokemon-details.page';
import { NotFoundPage } from './pages/not-found/not-found.page';
import { AuthGuard } from './guards/auth/auth.guard';

const routes: Routes = [
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'trainer/profile',
        component: TrainerProfilePage,
        canActivate: [AuthGuard]
    },
    {
        path: 'pokemon/catalogue',
        component: PokemonCataloguePage,
        canActivate: [AuthGuard]
    },
    {
        path: 'pokemon/:pId/details',
        component: PokemonDetailsPage
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: '**',
        component: NotFoundPage
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
