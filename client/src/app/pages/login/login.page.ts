import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.style.scss']
})
export class LoginPage {
    loginForm: FormGroup = new FormGroup({
        name: new FormControl('', Validators.required),
        //password: new FormControl('', Validators.required)
    });

    constructor(private authService: AuthService, private router: Router) {

    }
    
    get name() {
        return this.loginForm.get('name');
    }

    get password() {
        return this.loginForm.get('password');
    }

    login() {
        if (!this.loginForm.valid) {
            return;
        }
        console.log(`${this.name && this.name.value} is logging in with `); //password //${this.password && this.password.value}`);

        const { name } = this.loginForm.value;
        this.authService.login(name, '').subscribe((resp: any) => this.router.navigateByUrl('/trainer/profile'))
    }
}