import { Component } from '@angular/core';

@Component({
    selector: 'not-found',
    templateUrl: './not-found.page.html',
    styleUrls: ['./not-found.style.scss']
})
export class NotFoundPage {

}