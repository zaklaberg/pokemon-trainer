import { Component } from '@angular/core'
import { PokemonBase } from 'src/app/models/pokemon/base.model';
import { TrainerService } from 'src/app/services/pokemon/trainer.service';

@Component({
    selector: 'trainer-profile',
    templateUrl: './trainer-profile.page.html',
    styleUrls: ['./trainer-profile.style.scss']
})
export class TrainerProfilePage {
    pokemon: PokemonBase[] = [];

    constructor(private trainerService: TrainerService) {
        this.trainerService.ownedPokemon.subscribe(pokemon => this.pokemon = pokemon);
    }

    linkToDetails(pokemonId: number) {
        return `/pokemon/${pokemonId}/details/`;
    }
}