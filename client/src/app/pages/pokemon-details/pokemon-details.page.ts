import { Component } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { PokemonDetailsService } from 'src/app/services/pokemon/pokemon-details.service';
import { PokemonDetails }  from 'src/app/models/pokemon/details.model';
import { Observable } from 'rxjs';
import * as _ from "lodash";
import { TrainerService } from 'src/app/services/pokemon/trainer.service';
import { PokemonBase } from 'src/app/models/pokemon/base.model';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
    selector: 'pokemon-details',
    templateUrl: './pokemon-details.page.html',
    styleUrls: ['./pokemon-details.style.scss']
})
export class PokemonDetailsPage {
    public details: PokemonDetails = {} as PokemonDetails;

    constructor(
        private route: ActivatedRoute,
        public auth: AuthService,
        private detailsService: PokemonDetailsService,
        private trainerService: TrainerService) {
            const pokeId: number = parseInt(this.route.snapshot.paramMap.get('pId') || '', 10);

            this.detailsService.fetchDetailsById(pokeId)
            this.detailsService.details.subscribe(details => {
                this.details = details;
            });
    }

    ngOnInit() {
        this.details = {} as PokemonDetails;
    }

    collectPokemon() {
        this.trainerService.addPokemon(new PokemonBase(this.details.name, this.details.id))
            .subscribe(
                resp => {},
                error => {}
            );
    }

    get owned(): boolean {
        return this.trainerService.ownsPokemon(this.details.id);
    }

    get empty(): boolean {
        return _.isEmpty(this.details);
    }
}