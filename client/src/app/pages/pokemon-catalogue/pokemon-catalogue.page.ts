import { Component } from '@angular/core'
import { PokemonListService } from '../../services/pokemon/pokemon-list.service';
import { PokemonBase } from '../../models/pokemon/base.model';

@Component({
    selector: 'pokemon-catalogue',
    templateUrl: './pokemon-catalogue.page.html',
    styleUrls: ['./pokemon-catalogue.style.scss']
})
export class PokemonCataloguePage {
    pokemon: PokemonBase[] = [];

    constructor(public pokemonListService: PokemonListService) {
        this.pokemonListService.current().subscribe(pokemon => {
            this.pokemon = pokemon;
        })
    }
}