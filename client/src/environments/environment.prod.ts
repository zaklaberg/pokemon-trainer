export const environment = {
  production: true,
  trainerApiUrl: 'http://localhost:3000/api/v1/',
  pokeApiUrl: 'https://pokeapi.co/api/v2/'
};
