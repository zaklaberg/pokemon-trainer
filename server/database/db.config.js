const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

// Path in FileSync is relative to cwd
const adapter = new FileSync('./database/db.json')
const db = low(adapter)
db.defaults({ trainers: [] }).write()

module.exports = db