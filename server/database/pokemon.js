function addPokemon(trainer, pokemon) {
    trainer.get('pokemon')
        .push(pokemon)
        .write()
}

function getPokemon(trainer) {
    let pokemon = trainer.get('pokemon')
    return pokemon.value()
}

// Removes all pokemon from trainer. 
function deletePokemon(trainer) {
    trainer.get('pokemon')
    .remove()
    .write()
}

exports.addPokemon = addPokemon
exports.getPokemon = getPokemon
exports.deletePokemon = deletePokemon