const db = require('./db.config')
const crypto = require('crypto')

function generateSalt(rounds = 12) {
    return crypto.randomBytes(Math.ceil(rounds / 2)).toString('hex').slice(0, rounds);
}

function hasher(password, salt) {
    let hash = crypto.createHmac('sha512', salt)
    hash.update(password)
    let value = hash.digest('hex')
    
    return {
        salt: salt,
        hashedPassword: value
    }
}

function validateTrainer(name, password) {
    const trainer = db.get('trainers').find({name: name})
    if (!trainer.value()) {
        return false
    }

    const storedHash = trainer.get('hash').value()
    const computedHash = hasher(password, storedHash.hash)

    if (storedHash.hashedPassword === computedHash.hashedPassword) {
        return true
    }
    return false
}

function getTrainerById(trainerId) {
    let trainer = null
    try {
        trainer = db.get('trainers').find({id: trainerId})
        if (!trainer.value()) {
            throw new Error('Trainer not found.')
        }
    }
    catch(e) {
        return null
    }
    return trainer
}

function getTrainerByName(name) {
    const trainer = db.get('trainers').find({name: name})
    if (trainer.value()) {
        return trainer
    }
    return null
}


function addTrainer(trainer) {
    if (getTrainerByName(trainer.name) !== null) return false

    db.get('trainers')
    .push(trainer)
    .write()

    return true
}

exports.getTrainerById = getTrainerById
exports.getTrainerByName = getTrainerByName
exports.addTrainer = addTrainer