const router = require('express').Router({mergeParams: true})
const endecrypt = require('../../../endecrypt')
const nanoidLength = 8
const { nanoid } = require('nanoid')
const { getTrainerById } = require('../../../database/trainer')
const { 
    getPokemon, 
    addPokemon, 
    deletePokemon
} = require('../../../database/pokemon')

// Wrapper to exit with 404 if no trainer with corresponding id exists.
// If trainer exists, then trainer is added as third argument to handler.
// (This may possibly overwrite a third argument I never use. Should check!)
function exitOnInvalidTrainer(handler) {
    return (req, res) => {
        const trainerId = endecrypt.decrypt(req.params.trainerId)
        const trainer = getTrainerById(trainerId)
        if (trainer === null) {
            res.status(404).send({error: 'Trainer not found.'})
            return
        }
        handler(req, res, trainer)
    }
}

// Return all pokemon for trainer
router.get('/', exitOnInvalidTrainer((req, res, trainer) => {
    const pokemon = getPokemon(trainer)
    res.send(pokemon)
}))

// POST /api/
// Add a new pokemon
// Body is expected to be { name, id, imageUrl }
router.post('/', exitOnInvalidTrainer((req, res, trainer) => {
    // Return on invalid body
    let pokemon = req.body
    if (!pokemon || !pokemon.hasOwnProperty('name') || !pokemon.hasOwnProperty('id')) {
        res.status(400).send({error: "Body must contain {name: value, id: value}."})
        return
    }
    console.log(pokemon);
    // Add id, for future reasons (delete only particular pokemon, for example)
    pokemon = { name: pokemon.name, id: pokemon.id, imageUrl: pokemon.imageUrl }

    // Store pokemon in database
    addPokemon(trainer, pokemon)

    res.status(201).send({success: true})
}))

// DELETE /api/pokemon
// Deletes all pokemon
router.delete('/', exitOnInvalidTrainer((req, res, trainer) => {
    deletePokemon(trainer)
    res.status(200).send({success: true})
}))

module.exports = router