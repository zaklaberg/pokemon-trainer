const router = require('express').Router()
const endecrypt = require('../../endecrypt')
const nanoidLength = 8
const { nanoid } = require('nanoid')
const { 
    getTrainerById, 
    getTrainerByName, 
    addTrainer
} = require('../../database/trainer')

router.get('/:trainerId/exists', (req, res) => {
    const data = { exists: false }
    let trainer = null
    try {
        trainer = getTrainerById(endecrypt.decrypt(req.params.trainerId))
    } 
    catch(e) {}
    
    if (trainer === null) {
        res.status(404).send(data)
    }
    else {
        data.exists = true
        res.status(200).send(data)
    }
})

// POST /api/trainer/:name
// Dual purpose; since no pw. If trainer does not exist, is created
// Returns encrypted trainerID
// Body is expected to be { name: value }
router.post('/add', (req, res) => {
    // If body is bad, return
    if (!req.body.hasOwnProperty('name')) {
        res.status(403).send({error: 'Body must contain {name: value}.'})
        return
    }

    // If trainer exists, return id
    let trainer = getTrainerByName(req.body.name)
    if (trainer !== null) {
        res.status(200).send({id: endecrypt.encrypt(trainer.value().id)})
        return
    }

    // Trainer doesnt exist, add to database
    trainer = { name: req.body.name, id: nanoid(nanoidLength), pokemon: [] }
    addTrainer(trainer)
    
    // Return id and success!
    res.status(200).send({id: endecrypt.encrypt(trainer.id)})
})

router.use('/:trainerId/pokemon', require('./pokemon/pokemon.routes'))

module.exports = router