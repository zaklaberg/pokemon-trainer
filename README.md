# Glorious Pokemon Trainer
In this application you can view all the marvelous pokemon in existence. By selecting pokemon in the catalogue, you can also see entirely useless statistics like their height and weight, as well as all their moves, type and so on. 

There is even a button to collect them!

To run the app, start the server with `npm run server` in the server/ directory, then `ng serve` in the client/ directory.

Note that the app is highly secure and you are thus _required_ to enter a name before you can view and catch pokemon.

Gotta catch them all... or maybe one. 
